<?php get_header(); ?>

<section class="students-testimonial-area">
	<div class="container">
		<div class="sec-title white-title">
			<h2>What Our Happy Student Say</h2>
		</div>
		<div class="students-testimonial-content owl-carousel">
			<!-- Single Testimonial -->
			<?php
            $args = array( 'post_type' => 'testimonials', 'posts_per_page' =>-1, 'order' => 'ASC'  );
            $loop = new WP_query($args);                
            if($loop->have_posts()) {
            while($loop->have_posts()) {
            $loop->the_post(); 
            ?>

			<div class="students-single-testimonial">
				<div class="students-profile-round-1 students-profile-round"></div>
				<div class="students-profile-round-2 students-profile-round"></div>
				<p><?php echo get_the_content(); ?></p>
				<div class="client-info">
					<div class="client-images">
						<?php the_post_thumbnail('full',array('class'=>'img-fluid')); ?>
					</div>
					<div class="client-details">
						<h6><?php the_title(); ?></h6>
						<span><?php echo get_post_meta($post->ID,'testimonial_branch',true);?></span>
					</div>
				</div>

			</div>

			         <?php } } ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>