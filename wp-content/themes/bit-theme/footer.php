<footer>
  <div class="container">

    <div class="row footer-link-wrapper">

      <!--Footer about & social section-->

      <div class="col-xl-4 col-lg-12 col-md-12">
        <div class="about-business-wrapper">
          <div class="footer-logo">
            <a href="<?php echo home_url('/'); ?>"><img src="<?php bloginfo('template_directory'); ?>/assets/img/logo-1-.png" class="img-fluid" alt="footer-logo"></a>
          </div>
          <p>To establish leadership in learning, research and services and prepare students for Global Society</p>
          <div class="social-sec">
            <a href="https://www.facebook.com/bit.edu/" class="hvr-rotate" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="https://bit.edu.in/#" class="hvr-rotate" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="https://bit.edu.in/#" class="hvr-rotate" target="_blank"><i class="fa fa-youtube-play"></i></a>
          </div>
        </div>
      </div>

      <!--Footer quick link section-->

      <div class="col-xl-5 col-lg-8 col-md-7 col-sm-7">
        <div class="row">

        <!--Footer service link section-->

          <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div class="quick-links-wrapper">
              <h4>Quick Links</h4>
              <ul class="footer-nav">                
                <li><a href="#"> Placements</a></li>
                <li><a href="#">Pay Online</a></li>
                <li><a href="#">Career</a></li>
                <li><a href="#">Grievance</a></li>
                <li><a href="#">Alumni</a></li>
				        <li><a href="#">Academics</a></li>
                <li><a href="#">Admission</a></li>
				        <li><a href="#"> Anti Ragging Policy</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
     

     <div class="modal fade" id="bitvideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          X
        </button>
      <div class="modal-body">
       <iframe width="100%" height="400px"
src="https://www.youtube.com/embed/Vz6_5g0chbU">
</iframe>
      </div>
     
    </div>
  </div>
</div>
      <!--Footer Recent Post section-->

      <div class="col-xl-3 col-lg-4 col-md-5 col-sm-5">
        <div class="open-hrs-wrapper">
          <h4>Address:</h4>
          <div class="footer-openhrs">
            <strong class="cmp-add">Campus Address:</strong>
            <p>Infovalley, Harapur,
Bhubaneswar, Odisha- 752054, India</p>
            <strong class="cmp-add">Corporate Office:</strong>
            <p>Hig-30, In front of Hotel Pal Heights,
Jayadev Bihar Bhubaneswar, Odisha – 751013, India</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

    <!--Footer copyright section-->
<footer class="footer-bottom">
  <div class="container">
    <div class="row">
      <div class="copyright-wrapper">
        <p>© 2021 Bhubaneswar Institute of Technology (BIT). All Rights Reserved</p>
        <p>Designed by <a href="https://www.ajatus.in" class="author-link" target="blank">Ajatus Software</a></p>
      </div>
    </div>
  </div>
</footer>

<a href="#" class="back-to-top"><i class="fa fa-caret-up"></i></a>
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery.min.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery-ui.min.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/popper.min.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap.min.js"></script>
  <script src="<?php bloginfo('template_directory');?>/assets/js/jquery.magnific-popup.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/owl.carousel.min.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/aos.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/main.js"></script>
<script>
	   $(window).on('load', function () {
        $('.preloader').fadeOut(700);
    });
	
</script>

        <script>

$(".students-testimonial-content").owlCarousel({
    autoplay: false,
    dots: true,
    loop: true,
    items:2,
    nav:true,
    margin: 50,
              animateIn: 'fadeIn',
              animateOut: 'fadeOut',
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:2,
            nav:true,
            dots:true,
            loop:true
        }

    }
  });
        </script>
         <script>
         var siteMagnificPopup = function() {
        $('.image-popup').magnificPopup({
           type: 'image',
           closeOnContentClick: true,
           closeBtnInside: false,
           fixedContentPos: true,
           mainClass: 'mfp-no-margins mfp-with-zoom', 
          gallery: {
           enabled: true,
           navigateByImgClick: true,
           preload: [0,1] 
           },
           image: {
           verticalFit: true
           },
           zoom: {
           enabled: true,
           duration: 300 
           }
         });
         };
         siteMagnificPopup();
         
         $(document).ready(function() {
          $('.main_btn').on('click',function() {
            var IDS = $(this).data('id'); 
            $('#pname').val(IDS);
          });
          
        });
      </script>
<?php wp_footer(); ?>
</body>
</html>