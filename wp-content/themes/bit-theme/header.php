<!DOCTYPE html>
<html lang="en">
<head>
  <title>Emergence of the New BIT – on a path to the creation of the best engineering institute of
India</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/animate.min.css">
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/font-awesome.min.css">
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/hover.css">
   <link rel="stylesheet"  href="<?php bloginfo('template_directory'); ?>/assets/css/magnific-popup.css">
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/aos.css">
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style.css">
  <link rel="icon" href="<?php bloginfo('template_directory'); ?>/assets/img/logo-1-.png">
  <?php wp_head(); ?>
</head>

<body>
<div class="preloader">
    <img src="<?php bloginfo('template_directory'); ?>/assets/img/bit.gif">
</div>
<!--Topbar section-->
<section class="topbar-sec">
  <div class="container">
    <div class="row">
      <a href="<?php echo home_url('/'); ?>" class="navbar-brand" >
       <img src="<?php bloginfo('template_directory'); ?>/assets/img/logo-1-.png" class="img-fluid" alt="logo">
      </a>


      <div class="topbar-contact-section">
        <div class="call-wrapper">
          <div class="topbar-icon-sec">
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/mobile-phone.svg" class="img-fluid" alt="phone-icon">
          </div>
          <div class="topbar-cont-dtls-sec">
            <span>Call Us</span>
            <a href="tel:+91-9438486506"> +91-9438486506</a>
          </div>
        </div>

        <div class="email-wrapper">
          <div class="topbar-icon-sec">
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/open-email.svg" class="img-fluid" alt="mail-icon">
          </div>
          <div class="topbar-cont-dtls-sec">
            <span>Email Us</span>
            <a href="#">info@bit.edu.in</a>
          </div>
        </div>

        <div class="location-wrapper">
        <div class="social-ic-sec">
            <a href="https://www.facebook.com/bit.edu/" class="hvr-rotate" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="https://bit.edu.in/#" class="hvr-rotate" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="https://bit.edu.in/#" class="hvr-rotate" target="_blank"><i class="fa fa-youtube-play"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<!---Header section--->


  <header id="header">
    <div class="container">

      <div class="row">
        <div class="col-xl-12 theme-inner-header">
          
         <a href="index.html" class="logo mr-auto navbar-brand"><img src="" alt="" class="img-fluid"></a>

          <nav class="nav-menu d-none d-lg-block">
            <?php wp_nav_menu(array('theme_location'=>'primary','container'=>false,'menu_class'=>'')); ?>
            
          </nav>
            <div class="mob-menu-area"></div>
          <div class="request-quote-btn-wrapper">
          <a href="<?php echo home_url('contact-us');?>">Contact Us</a>
          </div>
        </div>
      </div>

    </div>
  </header>
  
<?php if(is_front_page() || is_home()) { ?>
  <!---Banner section--->

<div id="demo" class="carousel slide" data-ride="carousel">
    <ul class="carousel-indicators">
		 <?php
            $s=0;
          $args = array( 'post_type' => 'banner', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC'  );
          $loop = new WP_query($args);        
          if($loop->have_posts()) {
          while($loop->have_posts()) {
          $loop->the_post(); 
          ?>
    <li data-target="#demo" data-slide-to="<?php echo $s;?>" class="<?php if($s==0){echo 'active';} else{}?>"></li>
		<?php $s++;}  } ?>
 
  </ul>
    <div class="carousel-inner">
		 <?php
            $s=1;
          $args = array( 'post_type' => 'banner', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC'  );
          $loop = new WP_query($args);        
          if($loop->have_posts()) {
          while($loop->have_posts()) {
          $loop->the_post(); 
          $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($loop->post->ID), 'full' );
         
          ?>
   <div class="carousel-item <?php if($s==1){echo 'active';} else{}?>" style="background-image: url('<?php echo $backgroundImg[0];?>');"></div>
		  <?php $s++;}  } ?>
  </div>
  
  <div class="carousel-control-btn-sec">
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <i class="fa fa-chevron-left"></i>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <i class="fa fa-chevron-right"></i>
  </a>
</div>
</div>
	<div class="auto-container hosting-sec">
    <div class="row">
      <div class="col-lg-12"><h5>BIT is hosting</h5><a href="https://icacie.com/2021/" target="_blank"><h4 style="color:red">International Conference on Advanced Computing and Intelligent Engineering (Click Here)</h4></a></div>
    </div>
  </div>
   <?php } elseif(is_post_type_archive( 'courses' )) {
   ?>
<section class="train-banner-sec">
         <div class="auto-container">
            <div class="row">
               <div class="col-xl-12">
                  <h2 class="animate__animated animate__fadeInDown">Courses</h2>
               </div>
            </div>
         </div>
      </section>
   <?php
   } elseif(is_post_type_archive( 'notice' )) {
    ?>
<section class="train-banner-sec">
         <div class="auto-container">
            <div class="row">
               <div class="col-xl-12">
                  <h2 class="animate__animated animate__fadeInDown">Event / Placements / Notice</h2>
               </div>
            </div>
         </div>
      </section>
    <?php
    ?>
      <?php
   } elseif(is_post_type_archive( 'testimonials' )) {
    ?>
<section class="train-banner-sec">
         <div class="auto-container">
            <div class="row">
               <div class="col-xl-12">
                  <h2 class="animate__animated animate__fadeInDown">Testimonials</h2>
               </div>
            </div>
         </div>
      </section>
    <?php
   } else { ?>
      <section class="train-banner-sec">
         <div class="auto-container">
            <div class="row">
               <div class="col-xl-12">
                  <h2 class="animate__animated animate__fadeInDown"><?php echo get_the_title(); ?></h2>
               </div>
            </div>
         </div>
      </section>
      <?php }?>
