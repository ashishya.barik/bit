<?php get_header()?>

<section style="padding:40px 0">
<div class="container">
  <div class="row">


      <?php 
        $i = 1;
        $args = array(
            'post_type'=> 'courses',
            'posts_per_page'    => -1
        );              

        $the_query = new WP_Query( $args );
        if($the_query->have_posts() ) : 
        while ( $the_query->have_posts() ) : 
        $the_query->the_post(); 
       ?>
     <div class="col-md-4">
	     <div class="course-box">
			<div class="course-box-image"> 
				<div class="course-process-image">
					<?php the_post_thumbnail('full',array('class'=>'img-fluid')); ?>
					 <div class="course-num"><?php echo $i; ?></div>
				</div>								
			</div>
		   <div class="course-box-content">
			     <div class="course-box-title"><a href="<?php the_permalink(); ?>"><h5><?php the_title(); ?> </h5></a></div>
				<div class="course-box-description"><?php echo wp_trim_words(get_the_content(),'10','...'); ?></div>
			</div>
		</div>
        </div>

	     
	<?php
     $i++;
    endwhile; 
    wp_reset_postdata(); 
    else: 
    endif;

    ?> 
	  
	  </div>
</div>
	  
</section>

   <?php get_footer(); ?>