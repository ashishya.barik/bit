<?php get_header();?>
<section class="canteen-sec">
	<div class="container">

  <div class="row canteen-row-img">
			<div class="col-lg-6"><img src="https://mockups.ajatus.us/bit/wp-content/uploads/2021/08/Canteen.jpg" class="img-fluid canteen-card-img"></div>
			<div class="col-lg-6"><img src="https://mockups.ajatus.us/bit/wp-content/uploads/2021/08/Canteen_2.jpg" class="img-fluid canteen-card-img"></div>
		</div>

		<div class="row canteen-row">
			<div class="table-responsive">
		<table class="table canteen-table">
  <thead>
  	  <tr>
      <div class="time-tbl-sec">Timing Lunch 1pm to 3pm Snacks & Dinner 5pm to 8pm</div>
    </tr>
    <tr>
      <th scope="col">SL No.</th>
      <th scope="col">Item</th>
      <th scope="col">Half (Rs./-)</th>
      <th scope="col">Full (Rs./-)</th>
    </tr>
  </thead>
  <tbody>
				 
    <tr class="com-row-sec">
        <td>1</td>
      <td>Chili Chicken / Paneer / Potato</td>
       <td>60</td>
       <td>110</td>
          </tr>
    	  <tr class="com-row-sec">
        <td>2</td>
      <td>Pakora chicken / Paneer / veg</td>
       <td>50</td>
       <td>90</td>
          </tr>
            <tr class="com-row-sec">
        <td>3</td>
      <td>Manchurian chicken / Paneer / veg</td>
       <td>60</td>
       <td>110</td>
          </tr>
            <tr class="com-row-sec">
        <td>4</td>
      <td>Fried rice- veg</td>
       <td>50</td>
       <td>-</td>
          </tr>
            <tr class="com-row-sec">
        <td>5</td>
      <td>Fried rice- egg</td>
       <td>60</td>
       <td>-</td>
          </tr>
            <tr class="com-row-sec">
        <td>6</td>
      <td>Fried rice- chicken</td>
       <td>70</td>
       <td>-</td>
          </tr>
            <tr class="com-row-sec">
        <td>7</td>
      <td>Fried rice- mixed</td>
       <td>80</td>
       <td>-</td>
          </tr>
            <tr class="com-row-sec">
        <td>8</td>
      <td>Double egg omelette</td>
       <td>40</td>
       <td>-</td>
          </tr>	
            <tr class="com-row-sec">
        <td>9</td>
      <td>Butter Masala chicken / Paneer</td>
       <td>75</td>
       <td>-</td>
          </tr>	
            <tr class="com-row-sec">
        <td>10</td>
      <td>Dal Tadka / Dal fry</td>
       <td>40</td>
       <td>-</td>
          </tr>	
            <tr class="com-row-sec">
        <td>11</td>
      <td>Steam rice</td>
       <td>40</td>
       <td>-</td>
          </tr>	
            <tr class="com-row-sec">
        <td>12</td>
      <td>Chicken home style curry</td>
       <td>60</td>
       <td>-</td>
          </tr>	
            <tr class="com-row-sec">
        <td>13</td>
      <td>Crispy veg</td>
       <td>50</td>
       <td>-</td>
          </tr>		 
           <tr class="com-row-sec">
        <td>14</td>
      <td>Egg bhurji</td>
       <td>50</td>
       <td>-</td>
          </tr>		 
           <tr class="com-row-sec">
        <td>15</td>
      <td>Raita onion/mixed</td>
       <td>40</td>
       <td>-</td>
          </tr>		 
           <tr class="com-row-sec">
        <td>16</td>
      <td>Ice cream</td>
       <td>50</td>
       <td>2 scoop</td>
          </tr>		 
           <tr class="com-row-sec">
        <td>17</td>
      <td>Gulaab jamun / Rasgulla</td>
       <td>30</td>
       <td>2 piece</td>
          </tr>		 
           <tr class="com-row-sec">
        <td>18</td>
      <td>Masala Cold drink</td>
       <td>30</td>
       <td>1 glass</td>
          </tr>		 
        	 
    <tr class="com-row-sec">
        <td colspan="4" class="text-right">Parcel Cost 10 per dish</td>
      
          </tr>
  </tbody>
</table>	
		</div>
    </div>

	</div>
</section>

<?php get_footer();?>