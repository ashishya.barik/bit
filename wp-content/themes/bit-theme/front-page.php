<?php get_header(); ?>

<!-- About Us section -->
<section class="about-sec">
  <div class="auto-container">
    <div class="row">
 <div class="col-xl-5 col-lg-5"> 
     <div class="about-sec-thumb">
	          <div class="video-sec">
	<div class="site-heading">
          <h4>Webinar</h4>
		<a href="https://bit.edu.in/wp-content/uploads/2021/11/BIT-USMB-2021.pdf" target="_blank" rel="noopener"> <img src="http://localhost/bit-update/wp-content/uploads/2021/12/BIT-USMB-2021-thumb.jpg " class="img-fluid" alt=""></a>
        </div>
            </div>
	 </div>
 
      </div>

      <div class="col-xl-7 col-lg-7 about-content" data-aos="fade-down">
       <?php 
		 $my_postid = 263;//This is page id or post id
$content_post = get_post($my_postid);
$content = $content_post->post_content;
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]&gt;', $content);
echo $content; 
		 ?>

      </div>
     

    </div>
  </div>
</section>

<!-- Courses Section -->
<section class="course-section">
 <div class="auto-container departments-contet">
   <div class="row">
       <div class="col-xl-12" data-aos="fade-down">
        <div class="theme-heading-sec">
          <p>You Can Learn</p>
          <h2>Departments</h2>
        </div>
      </div>
	  <div class="owl-carousel course-slider" data-aos="fade-up">
      <?php 
$i = 1;
        $args = array(
            'post_type'=> 'courses',
            'posts_per_page'    => -1
        );              

        $the_query = new WP_Query( $args );
        if($the_query->have_posts() ) : 
        while ( $the_query->have_posts() ) : 
        $the_query->the_post(); 
       ?>
     <div class="item">
	     <div class="course-box">
			<div class="course-box-image"> 
				<div class="course-process-image">
          <a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail('full',array('class'=>'img-fluid')); ?></a>
					 <div class="course-num"><?php echo $i; ?></div>
				</div>								
			</div>
		   <div class="course-box-content">
			     <div class="course-box-title"><h5><?php the_title(); ?> </h5></div>
			</div>
		</div>
	  </div>

	     
	   <?php
     $i++;
    endwhile; 
    wp_reset_postdata(); 
else: 
endif;

?> 
	  
	  </div>
	  
   </div>
   <div class="row">
    <div class="col-md-12 text-center">
   <a href="<?php echo home_url('courses/'); ?>" class="theme-btn hvr-bounce-to-right">View All Departments</a>
 </div>
 </div>
 </div>
</section>
<section id="review" class="testimonial-sec">
  <div class="auto-container">
    <div class="row">
<!--------News Section--->
<div class="col-xl-5 col-lg-6 news-upcoming">
        <div class="site-heading">
          <h4>Event / Placements / Notice</h4>
        </div>
        <div class="news-wrapper">
			<marquee direction = "up" onMouseOver="this.stop()" onMouseOut="this.start()">
            <ul>
            <?php
            $args = array('post_type' => 'notice', 'posts_per_page' =>-1, 'order' => 'DESC');           
            $loop = new WP_query($args);                
            if($loop->have_posts()) {
            	while($loop->have_posts()) {
            		$loop->the_post(); 
            ?>

              <li><a href="#" class="more-content"><?php the_title(); ?></a> <span class="date"><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date('dS M Y',strtotime(get_post_meta($loop->post->ID,'notice_date',true))); ?></span></li>

            <?php } } ?>
              
            </ul>
				  </marquee>
        </div>
		<a href="<?php echo home_url('notice/'); ?>" class="news-box-btn hvr-bounce-to-right">View More</a>
      </div>
		
      <div class="col-xl-7 col-lg-6">
        <div class="testimonial-area">

        <div class="theme-heading-sec" data-aos="fade-down">
          <p>Testimonials </p>
          <h2>What Our Happy Student Say</h2>
        </div>

        <div class="owl-carousel testimonial-carousel" data-aos="fade-down">

            <?php
            $args = array( 'post_type' => 'testimonials', 'posts_per_page' =>-1, 'order' => 'ASC'  );
            $loop = new WP_query($args);                
            if($loop->have_posts()) {
            while($loop->have_posts()) {
            $loop->the_post(); 
            ?>
          
           <div class="item">
            <div class="testimonial-wrapper">
                <div class="testimonial-content-sec">
                  <p><?php echo get_the_content(); ?></p>
                </div>

                <div class="testimonial-bottom-sec">
                  <div class="testimonial-img-sec"><?php the_post_thumbnail('full',array('class'=>'img-fluid')); ?></div>
                  <div class="testimonial-title-sec"><h5><?php the_title(); ?></h5><p><?php echo get_post_meta($post->ID,'testimonial_branch',true);?></p></div>
                </div>

            </div>
          </div> 

         <?php } } ?>


        </div>
        <a href="<?php echo home_url('testimonials/'); ?>" class="theme-btn hvr-bounce-to-right">View All Reviews</a>
        </div>
      </div>
    </div>
    
  </div>
</section> 
<section class="recruiters-sec" data-aos="fade-up">
  <div class="auto-container">
    <div class="row">
	 <div class="col-xl-12">
        <div class="theme-heading-sec">
          <p>Placement</p>
          <h2>Our Top Recruiters</h2>
        </div>
      </div>
      <div class="col-xl-12">

        <div class="owl-carousel recruiters-carousel">

                       <?php
            $args = array( 'post_type' => 'recruiters', 'posts_per_page' =>-1, 'order' => 'DESC'  );
            $loop = new WP_query($args);                
            if($loop->have_posts()) {
            while($loop->have_posts()) {
            $loop->the_post(); 
            ?>

          <div class="item">
            <div class="recruiters-wrapper">
             <?php the_post_thumbnail('full',array('class'=>'img-fluid')); ?>
          </div>
          </div>
           <?php } } ?>

    </div>
  </div>
  </div>
	</div>
</section>
<section class="quote-form-sec animate__animated animate__fadeInUp">
  <div class="auto-container">
    <div class="row">
      <div class="col-xl-6 col-lg-6 col-md-12">
        <div class="video-sec">
         <a href="javascriptvoid(0)" data-toggle="modal" data-target="#bitvideo"> <div class="play-btn"><i class="fa fa-play"></i></div></a>
          <img src="<?php bloginfo('template_directory'); ?>/assets/img/contact-form2.jpg " class="img-fluid" alt="">
        </div>
      </div>
      <div id="quote-form" class="col-xl-6 col-lg-6 col-md-12" data-aos="fade-up">

        <div class="theme-heading-sec">
          <p>Get a quote</p>
          <h2>Use our form to get Admission</h2>
        </div>

        <div class="quote-form-wrapper" >
        <?php echo do_shortcode('[contact-form-7 id="452" title="Contact form for admission"]'); ?>
    </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>