<?php get_header(); ?>

<div class="container">
    <div class="row">
		<?php
			$args = array('post_type'=>'courses','posts_per_page'=>-1);
			$loop = new WP_Query($args);
			if($loop->have_posts()) : while($loop->have_posts()) : $loop->the_post();
            $customerdetails = get_post_meta($loop->post->ID,'interviewdetail',true);
            // print_r($customerdetails);
		?>
        <?php if(!empty($customerdetails)) { ?>
        <div class="col-lg-12">
            <div class="curriculum-content">
            <h5><?php echo get_the_title($loop->post->ID); ?></h5>
            <ul>
            <?php foreach($customerdetails as $cust) { ?>
                <li><a href="<?php echo $cust['description']; ?>" target="_blank"><?php echo $cust['title']; ?></a></li>
            <?php } ?>
               
            </ul>
            </div>
        </div>
<?php } ?>
      <?php endwhile; endif; wp_reset_postdata(); ?>  

        

        

        
        
    </div>
</div>

<?php get_footer(); ?>