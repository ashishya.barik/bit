<?php get_header(); ?>

<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="archive-notice">
				<ul>
					<?php
			            //$args = array( 'post_type' => 'notice', 'posts_per_page' =>-1,'order' => 'ASC'  );
			            $args = array(
				'post_type' => 'notice',
				'posts_per_page' =>-1,
				'meta_query' => array(
					array(
						'key' => 'notice_date',
            			'value' => date('dS M Y'),
            			'compare' => '<',
        			)
    			),
				'orderby' => 'meta_value_num',
    			'order'   => 'DESC'
			);
			            $loop = new WP_query($args);                
			            if($loop->have_posts()) {
			            while($loop->have_posts()) {
			            $loop->the_post(); 
            		?>
				 	<li data-aos="fade-up"><a href="#"><?php the_title(); ?></a> <span class="date"><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date('dS M Y',strtotime(get_post_meta($loop->post->ID,'notice_date',true))); ?></span></li>
				 	<?php } } ?>
				</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>