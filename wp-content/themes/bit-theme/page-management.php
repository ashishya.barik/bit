<?php get_header();?>
<section class="management-sec">
	<div class="container">
	<div class="row management-row" data-aos="fade-up">
 			<div class="col-lg-3 staff-img-sec">
 				<div class="staff-thumb-img">
 					<img src="https://mockups.ajatus.us/bit/wp-content/uploads/2021/08/avtar.jpg" class="img-fluid">
 				</div>
 			</div>
 			<div class="col-lg-9 staff-col-sec">
 				<div class="staff-container-sec">
 				<div class="staff-qualification">
 					<h3>Prof. Alok Dash</h3>
					<h4>CEO (FCA, MBA, M.Com)</h4>
 				</div>
 				<p>Our institute is committed to providing quality Education and Training in Engineering and Technology to prepare students for life and work equipping them to contribute to the technological, economic and social development of India.</p>
 			</div>
 		</div>
 		</div>
 		<div class="row management-row" data-aos="fade-up">
 			<div class="col-lg-3 staff-img-sec">
 				<div class="staff-thumb-img">
 					<img src="https://mockups.ajatus.us/bit/wp-content/uploads/2021/08/avtar.jpg" class="img-fluid">
 				</div>
 			</div>
 			<div class="col-lg-9 staff-col-sec">
 				<div class="staff-container-sec">
 				<div class="staff-qualification">
 					<h3>Dr. Amit Kumar Mishra</h3>
					<h4>Chairman</h4>
 				</div>
 				<p>Our vision is based on hard work, open communication, a strong emphasis on team work and a high level of responsibility.<br>“Education is for improving the lives of others and for leaving your community and world better than you found it”</p>
 			</div>
 		</div>
 		</div>
 		<div class="row management-row" data-aos="fade-up">
 			<div class="col-lg-3 staff-img-sec">
 				<div class="staff-thumb-img">
 					<img src="https://mockups.ajatus.us/bit/wp-content/uploads/2021/08/avtar.jpg" class="img-fluid">
 				</div>
 			</div>
 			<div class="col-lg-9 staff-col-sec">
 				<div class="staff-container-sec">
 				<div class="staff-qualification">
 					<h3>  Dr. Payodhar Padhi</h3>
					<h4>DIRECTOR (PhD from IIT, Kharagpur)</h4>
 				</div>
 				<p>The Institute is established to create, nurture, and shape technical professionals and leaders to create an inclusive and sustainable society in a national and international perspective. To achieve this vision, we launched undergraduate engineering degree programs that nurture many vibrant and promising professionals equipped with skills to face the ever changing social, economical and technical landscape of our country.</p>
 			</div>
 		</div>
 		</div>
 		
	</div>
</section>
<?php get_footer();?>