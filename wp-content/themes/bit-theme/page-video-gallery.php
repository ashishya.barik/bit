<?php get_header(); ?>
 <!-- Banner Section -->
  <section class="page-cont">
    <div class="container">
        <div class="row">
       
        <?php
        $args = array('post_type'=>'video','post_per_page'=>-1);
        $loop = new WP_Query($args);
        if($loop->have_posts()) : while($loop->have_posts()) : $loop->the_post();
                $uvideoDetails = get_post_meta($loop->post->ID,'uvideoDetails',true);
               
        ?>
            <?php
                $count = 0;
                foreach($uvideoDetails as $urls)
                {
                     $video_id = explode("?v=", $urls['name']);
                     $video_id = $video_id[1];
                ?>
                    
                 
                
                    <div class="col-lg-4 video-custom-col hvr-float">
                       <div class="youtubeplayer">
                           <iframe src="https://www.youtube.com/embed/<?php echo $video_id; ?>?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                       </div>
                    </div>
                
                <?php
                $count++;
                }
            ?>
       
        <?php endwhile; endif; wp_reset_query(); ?>
        </div>
    </div>
  </section>
<?php get_footer(); ?>