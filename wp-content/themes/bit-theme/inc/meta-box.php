<?php
add_action( 'add_meta_boxes', 'bit_metabox' );

function bit_metabox()
{ 
  add_meta_box( 'notice_', 'Notice Date', 'cd_meta_box_board_metabox', 'notice', 'normal', 'high' );
  add_meta_box( 'test-sec', 'Testimoniial Information', 'cd_meta_box_testimonial_metabox', 'testimonials', 'normal', 'high' );
  add_meta_box( 'test-sec', 'Board Information', 'cd_meta_box_advisory_board_metabox', 'advisory_board', 'normal', 'high' );
  
  
}


function cd_meta_box_testimonial_metabox($post)
{
    $values = get_post_custom( $post->ID );
    $testimonialbranch = isset( $values['testimonial_branch'] ) ? esc_attr( $values['testimonial_branch'][0] ) : '';

  wp_nonce_field( 'my_meta_box_nonce', 'meta_box_nonce' );
   ?>

    <p>
    <label style="font-size: 15px; line-height: 35px;">Branch</label>
    <input type="text" name="testimonial_branch" id="testimonial_branch" value="<?php echo $testimonialbranch; ?>"  style="width:100%" />
    </p>
    <?php
   }

function cd_meta_box_advisory_board_metabox($post)
{
    $values = get_post_custom( $post->ID );
    $boarddesignation = isset( $values['board_designation'] ) ? esc_attr( $values['board_designation'][0] ) : '';

  wp_nonce_field( 'my_meta_box_nonce', 'meta_box_nonce' );
   ?>

    <p>
    <label style="font-size: 15px; line-height: 35px;">Designation</label>
    <input type="text" name="board_designation" id="board_designation" value="<?php echo $boarddesignation; ?>"  style="width:100%" />
    </p>
    <?php
   }

function cd_meta_box_board_metabox($post)
{
    $values = get_post_custom( $post->ID );
    $position = isset( $values['notice_date'] ) ? esc_attr( $values['notice_date'][0] ) : '';

  wp_nonce_field( 'my_meta_box_nonce', 'meta_box_nonce' );
   ?>

    <p>
    <label style="font-size: 15px; line-height: 35px;">Date</label>
    <input type="date" name="notice_date" id="notice_date" value="<?php echo $position; ?>"  style="width:100%" />
    </p>
    <?php
   }

   

add_action( 'save_post', 'cd_meta_box_save' );
function cd_meta_box_save( $post_id )
{
  // Bail if we're doing an auto save
  if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
  
  // if our nonce isn't there, or we can't verify it, bail
  if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'my_meta_box_nonce' ) ) return;
  
  // if our current user can't edit this post, bail
  if( !current_user_can( 'edit_post' ) ) return;
  
 if ( isset ( $_POST['notice_date'] ) ) 
    update_post_meta( $post_id, 'notice_date', $_POST['notice_date'] );

  if ( isset ( $_POST['testimonial_branch'] ) ) 
    update_post_meta( $post_id, 'testimonial_branch', $_POST['testimonial_branch'] );
  if ( isset ( $_POST['board_designation'] ) ) 
    update_post_meta( $post_id, 'board_designation', $_POST['board_designation'] );
  
}


