<?php

function bit_posttype() {

  register_nav_menus(
    array(
      'primary-menu' => __( 'Primary Menu' ),
      'secondary-menu' => __( 'Secondary Menu' )
    )
  );
 
 register_post_type( 'video',
        array(
            'labels' => array(
                'name' => __( 'Video' ),
                'singular_name' => __( 'Video' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title','thumbnail'),
            'menu_icon'   => 'dashicons-video-alt3',
            'rewrite' => array('slug' => 'video'),
        )
 );
 
 register_post_type( 'gallery',
array(
  'labels' => array(
   'name' => __( 'Gallery' ),
   'singular_name' => __( 'Gallery' ),
	
  ),
  'public' => true,
  'has_archive' => true,
  'rewrite' => array('slug' => 'gallery'),
  'supports' => array('title','thumbnail', 'custom-fields'),
  'menu_icon'           => 'dashicons-images-alt2'
 )
);
register_post_type( 'banner', 
     array(
         'labels' => array(
             'name' => __( 'Banner ' ),
             'singular_name' => __( 'Banner' )
         ),
         'public' => true,
         'has_archive' => false,
         'supports' => 																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																		array('title','editor','thumbnail'),
         'menu_position'       =>50,
         'menu_icon'           => 'dashicons-format-gallery',
         'rewrite' => array('slug' => 'banner'),
         
     ));   

  register_post_type( 'courses',
array(
  'labels' => array(
   'name' => __( 'Courses' ),
   'singular_name' => __( 'Courses' ),
  
  ),
  'public' => true,
  'has_archive' => true,
  'rewrite' => array('slug' => 'courses'),
  'rewrite' => array('slug' => 'courses'),
  'supports' => array('title','thumbnail','editor'),
  'menu_icon'           => 'dashicons-book-alt'
 )
);
  flush_rewrite_rules();

    register_post_type( 'notice',
array(
  'labels' => array(
   'name' => __( 'Notice' ),
   'singular_name' => __( 'Notice' ),
  
  ),
  'public' => true,
  'has_archive' => true,
  'rewrite' => array('slug' => 'notice'),
  'rewrite' => array('slug' => 'notice'),
  'supports' => array('title','thumbnail','editor'),
  'menu_icon'           => 'dashicons-admin-page'
 )
);
  flush_rewrite_rules();

      register_post_type( 'recruiters',
array(
  'labels' => array(
   'name' => __( 'Recruiters' ),
   'singular_name' => __( 'Recruiters' ),
  
  ),
  'public' => true,
  'has_archive' => true,
  'rewrite' => array('slug' => 'recruiters'),
  'rewrite' => array('slug' => 'recruiters'),
  'supports' => array('title','thumbnail'),
  'menu_icon'           => 'dashicons-tickets-alt'
 )
);
  flush_rewrite_rules();

        register_post_type( 'testimonials',
array(
  'labels' => array(
   'name' => __( 'Testimonials' ),
   'singular_name' => __( 'Testimonials' ),
  
  ),
  'public' => true,
  'has_archive' => true,
  'rewrite' => array('slug' => 'testimonials'),
  'rewrite' => array('slug' => 'testimonials'),
  'supports' => array('title','thumbnail','editor'),
  'menu_icon'           => 'dashicons-twitch'
 )
);
  flush_rewrite_rules();

          register_post_type( 'advisory_board',
array(
  'labels' => array(
   'name' => __( 'Advisory Board' ),
   'singular_name' => __( 'Advisory Board' ),
  
  ),
  'public' => true,
  'has_archive' => true,
  'rewrite' => array('slug' => 'advisory_board'),
  'rewrite' => array('slug' => 'advisory_board'),
  'supports' => array('title','thumbnail'),
  'menu_icon'           => 'dashicons-networking'
 )
);
  flush_rewrite_rules();

}
 
add_action( 'init', 'bit_posttype' );

?>