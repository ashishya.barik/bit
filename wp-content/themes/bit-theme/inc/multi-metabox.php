<?php
add_action( 'add_meta_boxes', 'add_interview_meta' );

	/* Saving the data */
	add_action( 'save_post', 'interview_meta_save' );

	/* Adding the main meta box container to the post editor screen */
	function add_interview_meta() {
	    add_meta_box(
	        'semistar-details',
	       'Semestar',
	        'interview_details_init',
	        'courses');
	}

	
	function interview_details_init() {
	    global $post;
	    // Use nonce for verification
	    wp_nonce_field( plugin_basename( __FILE__ ), 'interview_nonce' );
	    ?>
	    <div id="interview_meta_item">
	    <?php

	    
	    $interviewdetail = get_post_meta($post->ID,'interviewdetail',true);
	    $c = 0;
	    if ( @count( $interviewdetail ) > 0 && is_array($interviewdetail)) {
	        foreach( $interviewdetail as $interviewdetails ) {
	            if ( isset( $interviewdetails['title'] ) || isset( $interviewdetails['description'])) {
	                printf( '<p>Semestar Name : <input type="text" name="interviewdetail[%1$s][title]" value="%2$s" />  
					
					 Syllabus Link  : <input type="text" name="interviewdetail[%1$s][description]" value="%3$s" /> 
					<a href="javascript:void(0)" class="remove-interview">%4$s</a></p>', $c, $interviewdetails['title'], $interviewdetails['description'],'Remove' );
	                $c = $c +1;
	            }
	        }
	    }

	    ?>
	<span id="output-interview"></span>
	<a href="javascript:void(0)" class="add_interview"><?php _e('Add Semestar'); ?></a>
	<script>
	    var $ =jQuery.noConflict();
	    $(document).ready(function() {
	        var count = <?php echo $c; ?>;
	        $(".add_interview").click(function() {
	            count = count + 1;

	            $('#output-interview').append('<p> Semestar Name :  <input type="text" name="interviewdetail['+count+'][title]" value="" />  Syllabus Link : <input type="text"  name="interviewdetail['+count+'][description]"  /> <a href="javascript:void(0)" class="remove-interview"><?php echo "Remove"; ?></a></p>' );
	            return false;
	        });
	       $(document.body).on('click','.remove-interview',function() {
	            $(this).parent().remove();
	        });
	    });
	    </script>
		<style>
			div#interview_meta_item input {
    width: 100%;
    height: 35px;
    margin-bottom: 20px;
}
			div#interview_meta_item textarea {
    width: 100%;
    height: 60px;
    margin-bottom: 13px;
}
			div#interview_meta_item p {
    background: #e2e2e2;
    padding: 20px;
    margin-bottom: 20px;
}
a.remove-interview {
    color: #fff;
    background: #ff0000;
    padding: 5px;
    text-decoration: none;
    text-transform: uppercase;
}
.add_interview {
    background: #f04b85;
    color: #fff;
    text-decoration: none;
    padding: 8px 12px;
    box-shadow: none;
    font-size: 16px;
}
div#interview_meta_item .add_interview:foucs{
	color: #fff !important;
  color: #fff !important;
  box-shadow:unset !important;
  outline: none !important;
}
div#interview_meta_item .add_interview:hover{
  color: #fff !important;
  color: #fff !important;
  box-shadow:unset !important;
  outline: none !important;
}
</style>

	</div><?php

	}

	/* Save function for the entered data */
	function interview_meta_save( $post_id ) {
	    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
	        return;
	    // Verifying the nonce
	    if ( !isset( $_POST['interview_nonce'] ) )
	        return;

	    if ( !wp_verify_nonce( $_POST['interview_nonce'], plugin_basename( __FILE__ ) ) )
	        return;
	    // Updating the interviewdetail meta data
	    $interviewdetail = $_POST['interviewdetail'];

	    update_post_meta($post_id,'interviewdetail',$interviewdetail);
	}
	


