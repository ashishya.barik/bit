<?php
add_action( 'add_meta_boxes', 'add_uvideo_meta' );

/* Saving the data */
add_action( 'save_post', 'uvideo_meta_save' );

/* Adding the main meta box container to the post editor screen */
function add_uvideo_meta() {
   add_meta_box(
       'customer-details',
      'YOUTUBE URL',
       'uvideo_details_init',
       'video');
}

/*Printing the box content */
function uvideo_details_init() {
   global $post;
   // Use nonce for verification
   wp_nonce_field( plugin_basename( __FILE__ ), 'vidoe_nonce' );
   ?>
   <div id="employee_meta_item">
   <?php

   //Obtaining the linked employeedetails meta values
   $uvideoDetails = get_post_meta($post->ID,'uvideoDetails',true);
   $c = 0;
   if ( count( $uvideoDetails ) > 0 && is_array($uvideoDetails)) {
       foreach( $uvideoDetails as $uvideoDetail ) {
           if ( isset( $uvideoDetail['name'] ) ) {
               printf( '<p><input type="text" name="uvideoDetails[%1$s][name]" value="%2$s" /> <a href="javascript:void(0)" class="remove-package">%3$s</a></p>', $c, $uvideoDetail['name'], 'Remove' );
               $c = $c +1;
           }
       }
   }

   ?>
<span id="output-package"></span>
<a href="javascript:void(0)" class="add_package"><?php _e('ADD URL'); ?></a>
<script>
   var $ =jQuery.noConflict();
   $(document).ready(function() {
       var count = <?php echo $c; ?>;
       $(".add_package").click(function() {
           count = count + 1;

           $('#output-package').append('<p><input type="text" name="uvideoDetails['+count+'][name]" value="" /> <a href="javascript:void(0)" class="remove-package"><?php echo "Remove"; ?></a></p>' );
           return false;
       });
      $(document.body).on('click','.remove-package',function() {
           $(this).parent().remove();
       });
   });
   </script>
<style>
a.remove-package {
    color: #fff;
    background: #ff0000;
    padding: 5px;
    text-decoration: none;
    text-transform: uppercase;
}
.add_package {
    background: #0073aa;
    color: #fff;
    text-decoration: none;
    padding: 5px;
}
#employee_meta_item input {
    width: 90%;
}

</style>

</div><?php

}

/* Save function for the entered data */
function uvideo_meta_save( $post_id ) {
   if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
       return;
   // Verifying the nonce
   if ( !isset( $_POST['vidoe_nonce'] ) )
       return;

   if ( !wp_verify_nonce( $_POST['vidoe_nonce'], plugin_basename( __FILE__ ) ) )
       return;
   // Updating the uvideoDetails meta data
   $uvideoDetails = $_POST['uvideoDetails'];

   update_post_meta($post_id,'uvideoDetails',$uvideoDetails);
}

?>