<?php
add_action('after_setup_theme','mythemesetup');
function mythemesetup()
{
    add_theme_support('post-thumbnails');
 
} 

require get_template_directory().'/inc/post-type.php';
require( get_template_directory()  .'/gallery.php' );
require get_template_directory().'/inc/video-metabox.php';
require get_template_directory().'/inc/meta-box.php';
require get_template_directory().'/inc/multi-metabox.php';

function custom_login_logo() {
	echo '<style type="text/css">
	h1 a { background-image: url('.get_bloginfo('template_directory').'/assets/img/admin-logo.png) !important; background-size: 131px !important; height:120px !important; width:120px !important; }
   body{  background: linear-gradient(to bottom, #8fa2a145, #4e82e3); height: 100vh;}
   .login #nav a {
      text-decoration: none;
      color: #fff !important;
  }
  .login #backtoblog a, .login #nav a {
   text-decoration: none;
   color: #fff !important;
}
	</style>';
}
add_action('login_head', 'custom_login_logo');

function bartag_func( $atts ) {
  $a = shortcode_atts( array(
    'id' => '0'
  ), $atts );

  ?>
  <div class="gal">
         <?php
        
            $args = array('post_type'=>'gallery','p'=>$a['id']);
            $the_query = new WP_Query($args);
            if($the_query->have_posts()) { while($the_query->have_posts()) { $the_query->the_post(); 
          $ids = get_post_meta($the_query->post->ID, 'vdw_gallery_id', true);
          foreach ($ids as $key => $value) : 
          $image = wp_get_attachment_image_src($value,'full');
          $thumb = wp_get_attachment_image_src($value, 'thumbnail');
         ?>
         
    <a class="image-popup" href="<?php echo $image[0]; ?>"  data-title="<?php echo get_the_title($value);?>"><img src="<?php echo $image[0]; ?>" ></a>
          
         
        
         <?php endforeach;  } } wp_reset_postdata(); ?>
         </div>
  <?php
}
add_shortcode( 'galleryvisible', 'bartag_func' );


function adivisorybord()
{
    ob_start();
?>
<div class="owl-carousel advisory-board-slider ">

                                <?php
                                    $args = array( 'post_type' => 'advisory_board', 'posts_per_page' =>-1, 'order' => 'ASC'  );
                                    $loop = new WP_query($args);                
                                    if($loop->have_posts()) {
                                    while($loop->have_posts()) {
                                    $loop->the_post(); 
                                ?>
                                <div class="item" >
                                    <div class="advisory-main">
                                          <div class="board-member" data-aos="fade-up">
                                            <div class="member-img">
                                                <?php the_post_thumbnail('full',array('class'=>'img-fluid')); ?>
                                            </div>
                                            <div class="board-member-info">
                                                 <h4><?php echo get_the_title($loop->post->ID); ?></h4>
                                                <span><?php echo get_post_meta($loop->post->ID,'board_designation',true);?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } } ?>
                            </div>
<?php

    return ob_get_clean();
}

add_shortcode('advisory','adivisorybord');

add_action('admin_menu','remove_menus');
function remove_menus()
{
	//remove_menu_page('themes.php');
	remove_menu_page('plugins.php');
	remove_menu_page('tools.php');
}

function remove_core_updates(){
    global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
    }
    add_filter('pre_site_transient_update_core','remove_core_updates');
    add_filter('pre_site_transient_update_plugins','remove_core_updates');
    add_filter('pre_site_transient_update_themes','remove_core_updates');