<?php get_header();?>

      <!-- Contact Details Section -->
      <section class="contact-add-sec">
         <div class="container">
            <div class="row bit-remodel-address-contact">
             
               <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="address-wrapper">
          <div class="bit-icsec"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
          <div class="bit-addsec">
            <h4>(BIT Campus Address)</h4>
            <p>Infovalley, Harapur, Bhubaneswar, Odisha<br> Pin- 752054, India<br>Email: info@bit.edu.in <br>Admission Cell: +91-9238321869 </p>
          
          </div>
         </div>
      </div>
             <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="address-wrapper">
          <div class="bit-icsec"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
          <div class="bit-addsec">
            <h4>(BIT Corporate Office)</h4>
            <p>HIGH-30, In front of Hotel Pal Heights, Jayadev Bihar Bhubaneswar,Odisha – 751013, India<br>Email: info@bit.edu.in</p>
          
          </div>
         </div>
      </div>
            </div>
         </div>
      </section>
      <!-- Contact Form Section -->
      <section class="contact-form">
         <div class="container">
            <div class="row">
               <div class="col-xl-12">
                  <div class="theme-heading-sec">
                     <h2>Use our form to get Admission</h2>
                    
                  </div>
               </div>
            </div>
            <div class="row contact-form-row">
               <div class="col-xl-6 col-lg-6" data-aos="fade-down">
                  <?php echo do_shortcode('[contact-form-7 id="453" title="Contact-us form"]'); ?>
               </div>
               <div class="col-xl-6 col-lg-6 map" data-aos="fade-up">
                  <div class="map-sec">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14973.753232844701!2d85.710923!3d20.240649000000005!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe4da6588ebcfb8a0!2sBhubaneswar%20Institute%20of%20Technology!5e0!3m2!1sen!2sin!4v1619501980194!5m2!1sen!2sin"></iframe>
                  </div>
               </div>
            </div>
         </div>
      </section>



<?php get_footer();?>