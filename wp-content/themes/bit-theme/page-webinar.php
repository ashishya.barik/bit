<?php /* Template Name: Webinar Page */ ?>

<?php get_header();?>

<section class="canteen-sec">
  <div class="container">

    <div class="row canteen-row">
		<div class="table-responsive">
    <table class="table canteen-table">
  <thead>
      <tr>
      <div class="time-tbl-sec">The details of webinars conducted</div>
    </tr>
    <tr>
      <th scope="col">SL No.</th>
      <th scope="col">Date</th>
      <th scope="col">Webinar Topic</th>
      <th scope="col">Speaker</th>
      <th scope="col">PDF Link</th>
    </tr>
  </thead>
  <tbody>
         <tr class="com-row-sec">
        <td>1</td>
      <td>20-11-2021</td>
       <td>Utilisation of Solid industrial wastes in Manufacture of Building Materials</td>
       <td>Dr. Syed Mohammed Mustakin</td>
       <td><a href="https://bit.edu.in/wp-content/uploads/2021/11/BIT-BAoN-2021.pdf" tareget="_blank" rel="noopener">PDF</a></td>
          </tr>
	  <tr class="com-row-sec">
        <td>2</td>
      <td>24-09-2021</td>
       <td>Virtual Power Plant</td>
       <td>Er. K.Sunil Patro</td>
       <td><a href="#" tareget="_blank">--</a></td>
          </tr>
	  <tr class="com-row-sec">
        <td>3</td>
      <td>04-09-2021</td>
       <td>Personal Branding for Students</td>
       <td><a>Mr. Kamala Kanta Dash</a></td>
       <td><a href="https://bit.edu.in/wp-content/uploads/2021/09/BIT-PDFS-2021-Poster.pdf" target="_blank" rel="noopener">PDF</a></td>
          </tr>
	  <tr class="com-row-sec">
        <td>4</td>
      <td>20-08-2021</td>
       <td>Supply Chain in Automobile Industry</td>
       <td>Er. Chandak Kumar Mishra</td>
       <td><a href="https://bit.edu.in/wp-content/uploads/2021/09/BIT-SCAI-Poster.pdf" target="_blank" rel="noopener">PDF</a></td>
          </tr>
	  <tr class="com-row-sec">
        <td>5</td>
      <td>07-08-2021</td>
       <td>Distributed Robotics Platforms and Consunses</td>
       <td>Dr. Priya Ranjan</td>
       <td><a href="https://bit.edu.in/wp-content/uploads/2021/09/BIT-DRPC-Poster.jpeg" target="_blank" rel="noopener">PDF</a></td>
          </tr>
	  <tr class="com-row-sec">
        <td>6</td>
      <td>24-07-2021</td>
       <td>A Developing Countru Prospective</td>
       <td>Dr. Binay Patnaik</td>
       <td><a href="https://bit.edu.in/wp-content/uploads/2021/09/BIT-Webinar-SCMT-2021-1.pdf" target="_blank" rel="noopener">PDF</a></td>
          </tr>
	  <tr class="com-row-sec">
        <td>7</td>
      <td>10-07-2021</td>
       <td>Multi Scale Modelling of Material Development</td>
       <td>Dr . Payodhar Padhy</td>
			<td><a href="#" tareget="_blank">--</a></td>
          </tr>
    <tr class="com-row-sec">
      <td>8</td>
      <td>26-06-2021</td>
      <td>Chenab Bridge</td>
      <td>Er. Rashmi Ranjan Mallick</td>
      <td><a href="#" tareget="_blank">--</a></td>
          </tr>
  </tbody>
</table>  
		</div>
    </div>

      <div class="row canteen-row-img">
      <div class="col-lg-6"><img src="https://bit.edu.in/wp-content/uploads/2021/11/BIT-USMB-2021-1-scaled.jpg" class="img-fluid webinar-card-img"></div>
    
    </div>

  </div>
</section>

<?php get_footer(); ?>