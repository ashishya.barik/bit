<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bit-update' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '0Y`apvrP+%[|.SIq5# |{rL 1=uMjzGdel-7Mks P&*<G|NjlDZ&fDL]Ae5PP#(L' );
define( 'SECURE_AUTH_KEY',  'zFCjJ`&Qi_kN{0zWR#4.$sueir$W:-&)k[[t;};,>Y4<DTb3,#,6}c F[0UO:Bp1' );
define( 'LOGGED_IN_KEY',    '7/Xv$Uek kcnL72~M^ow.@f%<,~lv[l<7O)Tb^mV:Im%U:T/3VViQy-AD%6X]|{C' );
define( 'NONCE_KEY',        'bxtH2e|Xu hmOuiio|W,)|8N7*nu$<j6SJx]rF2yz;z$NU]1kSP<0#0/ >|tG(3M' );
define( 'AUTH_SALT',        ']fh?#U}t[*Kgcj2YhQzr~yozq#ck+C!``?Hcxv9R4#F?VayiWJajRx;WEG-#(8zX' );
define( 'SECURE_AUTH_SALT', '<C9i1nbMZ*j/fksQ6zth1qln-F&[dv=,Mh[tx)h0VywT!T2`run%&Z>9lO-!Z#-A' );
define( 'LOGGED_IN_SALT',   'ys-Y]MezewKZL8Gvhw2g8HC*xD|]oN?Rib^!Wjl@u57[N!/$A0D%XwjAS6&*9%ai' );
define( 'NONCE_SALT',       'X&AUk_Z:gWjCp#FhZ0wC909^O@}$W/UO,*AI%5zkXw;ZeDXyco(|M:y->@-I*NzL' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
